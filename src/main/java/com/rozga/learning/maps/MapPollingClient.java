package com.rozga.learning.maps;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.IMap;
import lombok.SneakyThrows;

public class MapPollingClient {

    public static void main(String[] args) {
        System.out.println("[get thread] getting instance of the map");


        final Runnable target = new Runnable() {
            @SneakyThrows
            @Override
            public void run() {

                final IMap<Long, Account> accountIMap = HazelcastClient.newHazelcastClient().getMap("account-map");
                long cnt = 0;
                while (true) {

                    System.out.println(Thread.currentThread()+ " [get] starting polling");
                    // how about getOrDefault instead ??
                    final Account account = accountIMap.get(cnt);
                    if (account != null) {
                        System.out.println(Thread.currentThread()+ " [get] " + (++cnt) + " polled account ID: " + account.getAccountID());
                    } else {
                        System.out.println(Thread.currentThread()+ " [get] returned null");
                    }

                    Thread.sleep(1000);
                }
            }
        };

        new Thread(target).start();
        new Thread(target).start();

    }
}

package com.rozga.learning.maps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class AccountDao {

    private Map<Long, Account> dummyStore = new HashMap<>();

    public AccountDao() {
        //Random random = new Random();

        for (long i = 0; i < 200; i++) {
            dummyStore.put(i, new Account(i));
        }
    }


    public void save(Account account) {

    }

    public void saveAll(Collection<Account> values) {

    }

    public void delete(Account account) {

    }

    public Iterable<Account> findAll(Collection<Long> keys) {
        return keys.stream().map(key -> dummyStore.get(key)).collect(Collectors.toList());
    }

    public Account findOne(Long aLong) {
        return this.dummyStore.get(aLong);
    }

    public void deleteAll(Iterable<Account> accounts) {

    }

    public Iterable<Account> findAll() {
        return this.dummyStore.values();
    }
}

package com.rozga.learning.maps;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.core.Hazelcast;

public class HazelcastNodeWithMapStoreConfig2Node {

    public static void main(String[] args) {
        AccountDao accountDao = new AccountDao();

        MapStoreConfig accountStoreConfig = new MapStoreConfig();
        accountStoreConfig.setImplementation(new AccountMapStore(accountDao));
        accountStoreConfig.setEnabled(true);
        accountStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.LAZY);

        MapConfig accountConfig = new MapConfig();
        accountConfig.setName("account-map");
        accountConfig.setMapStoreConfig(accountStoreConfig);

        Config config = new Config();
        config.addMapConfig(accountConfig);

        Hazelcast.newHazelcastInstance(config);
        System.out.println("started new HZ instance");
    }

}

package com.rozga.learning;

import java.util.*;

public class TransactionDao {

    private Map<Long, Transaction> dummyStore = new HashMap<>();

    public TransactionDao() {

        Random random = new Random();

        for (int i = 0; i < 30; i++) {
            dummyStore.put(random.nextLong(), new Transaction("tx_" + i));
        }
    }

    public void save(Long aLong, Transaction transaction) {
        System.out.println("[dao] save");
        dummyStore.put(aLong, transaction);
    }

    public void saveAll(Map<Long, Transaction> map) {
        System.out.println("[dao] save ALL");
        dummyStore.putAll(map);
    }

    public void delete(Long aLong) {
        System.out.println("[dao] delete");
        dummyStore.remove(aLong);
    }

    public void deleteAll(Collection<Long> collection) {
        System.out.println("[dao] delete ALL");
        collection.stream().forEach(key -> {
            dummyStore.remove(key);
        });
    }

    public Transaction findByKey(Long aLong) {
        System.out.println("[dao] findByKey: " + aLong);
        return dummyStore.get(aLong);
    }

    public Map<Long, Transaction> findAllByKeys(Collection<Long> keys) {
        System.out.println("[dao] findByAllKeys: " + keys);
        Map<Long, Transaction> result = new HashMap<>();

        keys.stream().forEach(key -> {
            final Transaction transaction = dummyStore.get(key);

            if (transaction != null) {
                result.put(key, transaction);
            }
        });

        return result;
    }

    public Set<Long> findAllKeys() {
        System.out.println("[dao] findAllKeys");
        return dummyStore.keySet();
    }
}

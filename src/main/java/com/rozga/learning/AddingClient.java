package com.rozga.learning;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.IQueue;

public class AddingClient {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("[adding thread] getting instance of the queue");
        final IQueue<Transaction> txQueue = HazelcastClient.newHazelcastClient().getQueue("transaction-queue");

        int i = 0;
        while (true) {

            System.out.println("[adding] elem: added_" + i);
            txQueue.put(new Transaction("added_" + i++));
            System.out.println("[adding] done");
            Thread.sleep(1000);
        }
    }
}

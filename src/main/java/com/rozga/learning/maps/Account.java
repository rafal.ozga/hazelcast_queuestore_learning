package com.rozga.learning.maps;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {

    @Getter
    private Long accountID;
}

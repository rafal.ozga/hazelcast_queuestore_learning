package com.rozga.learning.maps;

import com.hazelcast.core.MapStore;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@AllArgsConstructor
public class AccountMapStore implements MapStore<Long, Account> {

    private AccountDao accountDao;

    @Override
    public void store(Long aLong, Account account) {
        accountDao.save(account);
    }

    @Override
    public void storeAll(Map<Long, Account> map) {
        accountDao.saveAll(map.values());
    }

    @Override
    public void delete(Long aLong) {
        Account account = this.load(aLong);
        accountDao.delete(account);
    }

    @Override
    public void deleteAll(Collection<Long> keys) {
        Iterable<Account> accounts = accountDao.findAll(keys);
        accountDao.deleteAll(accounts);
    }

    @SneakyThrows
    @Override
    public Account load(Long aLong) {
        TimeUnit.SECONDS.sleep(10);
        return accountDao.findOne(aLong);
    }

    @Override
    public Map<Long, Account> loadAll(Collection<Long> keys) {
        Iterable<Account> accounts = accountDao.findAll(keys);
        return StreamSupport.stream(accounts.spliterator(), false).collect(Collectors.toMap(Account::getAccountID, Function.identity()));
    }

    @SneakyThrows
    @Override
    public Iterable<Long> loadAllKeys() {
        System.out.println("loading all keys");
        Iterable<Account> accounts = accountDao.findAll();
        Thread.sleep(30000);
        return StreamSupport.stream(accounts.spliterator(), false)
                .map(Account::getAccountID)
                .collect(Collectors.toList());
    }
}

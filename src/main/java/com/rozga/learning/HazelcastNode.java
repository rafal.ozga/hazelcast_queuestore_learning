package com.rozga.learning;

import com.hazelcast.config.Config;
import com.hazelcast.config.QueueConfig;
import com.hazelcast.config.QueueStoreConfig;
import com.hazelcast.core.Hazelcast;

public class HazelcastNode {

    public static void main(String[] args) {
        TransactionDao transactionDao = new TransactionDao();

        Config config = new Config();
        QueueConfig txQueueConfig = new QueueConfig();
        txQueueConfig.setName("transaction-queue");
        QueueStoreConfig txQueueStoreConfig = new QueueStoreConfig();
        txQueueStoreConfig.setStoreImplementation(new TxQueueStore(transactionDao));

        txQueueConfig.setQueueStoreConfig(txQueueStoreConfig);
        config.addQueueConfig(txQueueConfig);

        Hazelcast.newHazelcastInstance(config);
        System.out.println("started new HZ instance");
    }
}

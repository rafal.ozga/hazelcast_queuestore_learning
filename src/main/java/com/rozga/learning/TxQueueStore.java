package com.rozga.learning;

import com.hazelcast.core.QueueStore;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

@AllArgsConstructor
public class TxQueueStore implements QueueStore<Transaction> {

    private TransactionDao txDao;

    @Override
    public void store(Long aLong, Transaction transaction) {
        System.out.println("[store] store");
        txDao.save(aLong, transaction);
    }

    @Override
    public void storeAll(Map<Long, Transaction> map) {
        System.out.println("[store] storeAll");
        txDao.saveAll(map);
    }

    @Override
    public void delete(Long aLong) {
        System.out.println("[store] delete");
        txDao.delete(aLong);
    }

    @Override
    public void deleteAll(Collection<Long> collection) {
        System.out.println("[store] deleteAll");
        txDao.deleteAll(collection);
    }

    @Override
    public Transaction load(Long aLong) {
        System.out.println("[store] load");
        return txDao.findByKey(aLong);
    }

    @SneakyThrows
    @Override
    public Map<Long, Transaction> loadAll(Collection<Long> collection) {
        System.out.println("[store] loadALl");
        Thread.sleep(1000);
        System.out.println("[store] loadALl LOADED");
        return txDao.findAllByKeys(collection);
    }

    @SneakyThrows
    @Override
    public Set<Long> loadAllKeys() {
        System.out.println("[store] loadALlKeys");
        Thread.sleep(30000);
        System.out.println("[store] loadALlKeys LOADED");
        return txDao.findAllKeys();
    }
}

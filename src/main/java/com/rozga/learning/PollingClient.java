package com.rozga.learning;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.IQueue;
import lombok.SneakyThrows;

import java.util.concurrent.TimeUnit;

public class PollingClient {

    @SneakyThrows
    public static void main(String[] args) {

        System.out.println("[polling thread] getting instance of the queue");
        final IQueue<Object> txQueue = HazelcastClient.newHazelcastClient().getQueue("transaction-queue");
        int cnt = 0;
        while (true) {

            System.out.println("[poll] starting polling");
            Transaction tx = (Transaction) txQueue.poll(1000, TimeUnit.MILLISECONDS);
            if (tx != null) {
                System.out.println("[poll] " + (++cnt) + " polled tx ID: " + tx.getTransactioniD());
            } else {
                System.out.println("[poll] returned null");
            }
        }
    }

}
